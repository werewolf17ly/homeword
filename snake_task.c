#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct list_member {
    int value;
    uint32_t next;
};

int main() {
    struct list_member source;
    FILE *l_file;

    if ((l_file = fopen("binary.bin", "rb")) == NULL) {
        printf("Error!");
        exit(1);
    }

    while (source.next) {
        fread(&source, sizeof(struct list_member), 1, l_file);
        printf("Value: %d\tNext: %d\n", source.value, source.next);
    }

    fclose(l_file);
    return 0;
}