#include<stdio.h>

void bin(int n, int k) {
    if ((n >> k) & 1)
        printf(" 1");
    else
        printf(" 0");
}

typedef union {
    float f;
    struct {
        unsigned int mantissa: 23;
        unsigned int exponent: 8;
        unsigned int sign: 1;

    } raw;
} gul_float;

void sol(int n, int i) {
    int k;
    for (k = i - 1; k >= 0; k--)
        bin(n, k);
}

int main() {
    float f;
    scanf("%f", &f);
    gul_float beautiful_float;
    beautiful_float.f = f;
    printf(" |");
    printf("%d|", beautiful_float.raw.sign);
    sol(beautiful_float.raw.exponent, 8);
    printf(" |");
    sol(beautiful_float.raw.mantissa, 23);
    printf(" |");
    printf("\n");
    printf (" |s|      exp        |                    mantissa"
            "                   |\n\n");
}
